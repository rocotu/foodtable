import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/views/widgets/menu_item.dart';
import 'package:flutter_application_1/views/orden.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int mesa = 1;
  String mesaId = "xd";
  double sum = 0;
  List<String> selectedItems = [];
  List<String> selectedIds = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              InkWell(
                onTap: () {
                  showDialog(
                    context: context,
                    builder: (BuildContext context) {
                      return AlertDialog(
                        title: const Text('Mesa'),
                        content: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            const Text('Seleccionar número de mesa'),
                            StreamBuilder<QuerySnapshot>(
                              stream: FirebaseFirestore.instance
                                  .collection('mesas')
                                  .snapshots(),
                              builder: (context, snapshot) {
                                if (snapshot.connectionState ==
                                    ConnectionState.waiting) {
                                  return const CircularProgressIndicator();
                                }
                                if (snapshot.hasData) {
                                  final mesas = snapshot.data!.docs;
                                  return DropdownButton<int>(
                                    value: mesa,
                                    onChanged: (value) {
                                      setState(() {
                                        mesa = value!;
                                        mesaId = mesas[value].id;
                                      });
                                    },
                                    items: mesas.map((doc) {
                                      final mesaNum = doc['numero'] as int;
                                      return DropdownMenuItem<int>(
                                        value: mesaNum,
                                        child: Text(mesaNum.toString()),
                                      );
                                    }).toList(),
                                    menuMaxHeight: 200,
                                  );
                                }
                                return const Text('404!');
                              },
                            ),
                          ],
                        ),
                        actions: <Widget>[
                          TextButton(
                            child: const Text('Cerrar'),
                            onPressed: () {
                              Navigator.of(context).pop();
                            },
                          ),
                        ],
                      );
                    },
                  );
                },
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.restaurant_menu),
                    SizedBox(width: 10),
                    Text('Menu Item')
                  ],
                ),
              ),
            ],
          ),
          actions: [
            IconButton(
              icon: const Icon(Icons.credit_card),
              onPressed: () async {
                final result = await Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Orden(
                        selectedItems: selectedItems,
                        selectedIds: selectedIds,
                        mesaId: mesaId,
                        sum: sum),
                  ),
                );
                if (result) {
                  selectedItems = [];
                  selectedIds = [];
                  sum = 0;
                }
              },
            ),
          ],
        ),
        body: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const SizedBox(
                  height: 20,
                ),
                Expanded(
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestore.instance
                        .collection("productos")
                        .snapshots(),
                    builder: (context, AsyncSnapshot snapshot) {
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                      if (snapshot.hasData) {
                        return GridView(
                          gridDelegate:
                              const SliverGridDelegateWithMaxCrossAxisExtent(
                                  maxCrossAxisExtent: 300),
                          children: snapshot.data!.docs
                              .map((note) => MenuItem(
                                    note,
                                    onTapCallback: (nombre, id, price) {
                                      setState(() {
                                        selectedItems.add(nombre);
                                        selectedIds.add(id);
                                        sum += price;
                                      });
                                    },
                                  ))
                              .toList()
                              .cast<Widget>(),
                        );
                      }
                      return const Text("fuck u");
                    },
                  ),
                )
              ],
            )));
  }
}
