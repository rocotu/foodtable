import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Orden extends StatelessWidget {
  final List<String> selectedItems;
  final List<String> selectedIds;
  final String mesaId;
  final double sum;
  const Orden(
      {required this.selectedItems,
      required this.selectedIds,
      required this.mesaId,
      required this.sum,
      Key? key})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Orden'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: ListView.builder(
        itemCount: selectedItems.length,
        itemBuilder: (context, index) {
          return ListTile(
            title: Text(selectedItems[index]),
          );
        },
      ),
      bottomNavigationBar: BottomAppBar(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              IconButton(
                icon: const Icon(Icons.clear),
                onPressed: () {
                  Navigator.pop(context, true);
                },
              ),
              IconButton(
                icon: const Icon(Icons.add),
                onPressed: () async {
                  if (selectedIds.isEmpty) Navigator.pop(context);
                  final mesaQ = await FirebaseFirestore.instance
                      .collection('mesas')
                      .doc(mesaId)
                      .get();
                  final ordenQ = mesaQ.data()?['orden'];
                  if (ordenQ == null || ordenQ.isEmpty) {
                    Map<String, dynamic> dataNew = {
                      'fecha': DateTime.now(),
                      'mesa': mesaId,
                      'nota': "[[little sponge]] who hates its [[\$4.99]] life",
                      'productos': selectedIds,
                      'subtotal': sum,
                    };
                    final newOrden = await FirebaseFirestore.instance
                        .collection('ordenes')
                        .add(dataNew);
                    await FirebaseFirestore.instance
                        .collection('mesas')
                        .doc(mesaId)
                        .update({'orden': newOrden.id});
                  } else {
                    final ordenPos = FirebaseFirestore.instance
                        .collection('ordenes')
                        .doc(ordenQ);
                    final ordenPre = await ordenPos.get();
                    final oldData = ordenPre.data();
                    final dataNew = {
                      'fecha': oldData!['fecha'],
                      'mesa': oldData['mesa'],
                      'notas': oldData['nota'],
                      'productos': oldData['productos'] + selectedIds,
                      'subtotal': oldData['subtotal'] + sum,
                    };
                    await ordenPos.set(dataNew);
                  }
                  Navigator.pop(context, true);
                },
              ),
              IconButton(
                icon: const Icon(Icons.attach_money),
                onPressed: () {
                  Navigator.pop(context, true);
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
